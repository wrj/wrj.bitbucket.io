+++
date = "2017-06-07T00:48:08+07:00"
title = "static website with bitbucket.io for free"

+++

# Static site generator : Jekyll or Hugo?

I've seen many static websites that hosted on github.com for awhile. For me, I've been using BitBucket for many years since Mercurial days. Right now, all new projects are using Git exclusively.

Many goodies of Github still make me jealous. _Gist_, for example. _BitBucket's Snippet_ still not wide spread implemenation in many popular IDEs, Text Editors. So, that's pitty.

Now, static hosting got my attention. Since I'd like to create and maintain a simple website (you're looking at the website!). I'd like to use any kind of simple tool/cms.. even Wordpress is still too much to me. That's why I've reviewed again on how to publish static website here at __Bitbucket.io__ (_NOTE: Not a .COM_)

After looking at [Publishing a Website on Bitbucket Cloud](https://confluence.atlassian.com/bitbucket/publishing-a-website-on-bitbucket-cloud-221449776.html). 

_tl;dr_

1. Create a repository with the name "__YOURACCOUNT__.bitbucket.io". Yes _include_ the domain part in your repository too.
2. Using git to clone the blank repository as usual.

    * Jekyll : Ruby based, mature, many good themes. A bit difficult to install for average user.
    * Hugo : Very simple installation, mature, _fast site generation_ and still have many simple themes to choose

3. Then you can craft your own html pages, or using a few static site generator to help this task more pleasure in the long run.

You need to know basic command line commands, depends on which one you choose + git commands.

Hope this will be helpful for people who're looking for the same solution.

wrj
###### Wed Jun 7 01:05:58 ICT 2017